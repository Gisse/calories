'use strict';

angular.module('Calories', [
	'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'ui.bootstrap',
    'angularSpinner'])
.config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'view/login.html',
        controller: 'LoginControler'
      })
      .when('/login', {
        templateUrl: 'view/login.html',
        controller: 'LoginControler'
      })
      .when('/register', {
        templateUrl: 'view/register.html',
        controller: 'RegistrationControler'
      })
      .when('/preferences',{
      	templateUrl: 'view/preferences.html',
      	controller: 'PreferencesControler'
      })
      .when('/main',{
      	templateUrl: 'view/main.html',
      	controller: 'MainPageControler'
      })
      .when('/meal',{
      	templateUrl: 'view/addentrie.html',
      	controller: 'MealControler'
      })
      .otherwise({
        redirectTo: '/'
      });
});