'use strict';

angular.module('Calories').controller('LoginControler', function($scope, $http, $cookieStore, $location) {
	$scope.inputUsername
	$scope.inputPassword
	$scope.errormessage

	$scope.loading = false;

	var init = function() {
		var id = $cookieStore.get('id');
		if ( typeof id != 'undefined') {
			$location.path('/main');
		}
	};

	$scope.init = function() {
		console.log("Init LoginControler");
		var id = $cookieStore.get('id');
		console.log("Cookie id: " + id);
		if (id === null) {
			console.log("Cookie null");
		} else {
			console.log("Cookie not null");
		}
	};

	$scope.Login = function() {
		$scope.loading = true;
		var error = true;
		var data = {
			'username' : $scope.inputUsername,
			'password' : $scope.inputPassword
		};
		$http({
			url : 'http://37.139.13.79:9001/login',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.loading = false;
						$scope.errormessage = v;
					}
				} else {
					if (k === "id") {
						$cookieStore.put('id', v);
						$scope.loading = false;
						$location.path('/main');
					}
				}
			});
		}).error(function(err) {
			$scope.loading = false;
			$scope.errormessage = "Error during server comunication";
		});
	};
	init();
}).controller('RegistrationControler', function($scope, $http, $cookieStore, $location) {
	$scope.inputUsername
	$scope.inputPassword
	$scope.inputRepeatPassword
	$scope.errormessage
	$scope.loading = false;

	$scope.checkPassword = function() {
		$scope.registerform.inputRepeatPassword.$error.dontMatch = $scope.inputPassword !== $scope.inputRepeatPassword;
	};

	var init = function() {
		var id = $cookieStore.get('id');
		if ( typeof id != 'undefined') {
			$location.path('/main');
		}
	};

	$scope.Register = function() {
		$scope.loading = true;
		var error = true;
		var data = {
			'username' : $scope.inputUsername,
			'password' : $scope.inputPassword
		};
		$http({
			url : 'http://37.139.13.79:9001/register',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.loading = false;
						$scope.errormessage = v;
					}
				} else {
					if (k === "id") {
						console.log("God id for user: " + v);
						$cookieStore.put('id', v);
						$scope.loading = false;
						$location.path('/main');
					}
				}
			});
		}).error(function(err) {
			console.log('err', err);
			$scope.loading = false;
			$scope.errormessage = "Error during server comunication";
		});
	};
	init();

}).controller('PreferencesControler', function($scope, $http, $cookieStore, $location) {

	$scope.preferences
	$scope.caloriespreferences

	var init = function() {
		var id = $cookieStore.get('id');
		if ( typeof id == 'undefined') {
			$location.path('/login');
		}
		$scope.getUserPreferences();
	};

	$scope.LogOut = function() {
		$cookieStore.remove('id');
		$location.path('/');
	};

	$scope.getUserPreferences = function() {
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id')
		};
		$http({
			url : 'http://37.139.13.79:9001/preferences',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				} else {
					if (k === "daylimit") {
						console.log("daylimit for user: " + v);
						$scope.preferences = v;
					}
				}
			});
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
		});
	};

	$scope.saveUserPreferences = function() {
		console.log("saveUserPreferences: " + $scope.caloriespreferences);
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id'),
			'daylimit' : $scope.caloriespreferences
		};
		$http({
			url : 'http://37.139.13.79:9001/setpreferences',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				} else {
					if (k === "daylimit") {
						console.log("daylimit: " + v);
						$scope.preferences = v;
					}
				}
			});
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
		});
	};

	init();
}).controller('MainPageControler', function($scope, $http, $cookieStore, $location, $filter) {
	// init
	$scope.sort = {
		sortingOrder : 'id',
		reverse : false
	};

	$scope.maxSize = 5;

	$scope.gap = 5;

	$scope.itemsPerPage = 5;
	$scope.pagedItems = [];
	$scope.currentPage = 1;
	$scope.items = [];
	$scope.headers = [];
	$scope.totalItems = $scope.items.length;

	$scope.dataForPage = [];

	$scope.calorypreferences

	$scope.timeFrom = new Date();
	$scope.timeTo = new Date();

	$scope.hstep = 1;
	$scope.mstep = 5;

	$scope.ismeridian = true;

	$scope.noentries = false;
	$scope.notime = false;

	$scope.format = 'dd-MMMM-yyyy';

	$scope.minDate1
	$scope.minDate2

	$scope.dateto = new Date();
	$scope.datefrom = new Date();

	$scope.open1 = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.opened1 = true;
	};

	$scope.open2 = function($event) {
		$event.preventDefault();
		$event.stopPropagation();

		$scope.opened2 = true;
	};

	$scope.toggleMode = function() {
		$scope.ismeridian = !$scope.ismeridian;
	};

	// calculate page in place
	$scope.groupToPages = function() {
		$scope.pagedItems = [];
		console.log("$scope.headers.length: " + $scope.headers.length);
		for (var i = 0; i < $scope.headers.length; i++) {
			if (i % $scope.itemsPerPage === 0) {
				$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)] = [$scope.headers[i]];
			} else {
				$scope.pagedItems[Math.floor(i / $scope.itemsPerPage)].push($scope.headers[i]);
			}
		}
		console.log("$scope.pagedItems.length: " + $scope.pagedItems.length);
		console.log("$scope.pagedItems: " + $scope.pagedItems);
	};

	var init = function() {
		console.log("call for init");
		var id = $cookieStore.get('id');
		if ( typeof id == 'undefined') {
			$location.path('/login');
		}
		$scope.getUserPreferences();

	};

	$scope.LogOut = function() {
		$cookieStore.remove('id');
		$location.path('/');
	};

	$scope.getUserEntries = function() {
		$scope.headers = [];
		$scope.items = [];
		$scope.dataForPage = [];
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id')
		};
		$http({
			url : 'http://37.139.13.79:9001/getentrie',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				}
			});

			if (error === false) {
				$.each(response, function(k, v) {
					if (v !== "OK") {
						$scope.items.push(v);
					}
				});
				var inHeaders = false;
				$.each($scope.items, function(k, v) {
					$.each($scope.headers, function(key, value) {
						if (v["entry_date"] === value) {
							inHeaders = true;
						}
					});
					if (!inHeaders) {
						$scope.headers.push(v["entry_date"]);
					}
					inHeaders = false;

				});
				$.each($scope.headers, function(k, v) {
					var helper = [];
					var calories = 0;
					$.each($scope.items, function(key, value) {
						if (v === value["entry_date"]) {
							calories = calories + value["calories"];
						}
					});
					var caloriesdata = {
						'totalcalories' : calories
					};
					helper.push(caloriesdata);
					$.each($scope.items, function(key, value) {
						if (v === value["entry_date"]) {
							helper.push(value);
						}
					});
					$scope.dataForPage.push(helper);
				});
			}
			console.log("$scope.items.length: " + $scope.items.length);
			if ($scope.items.length > 0) {
				$scope.noentries = false;
				$scope.notime = false;
			} else {
				$scope.noentries = true;

			}
			$scope.groupToPages();
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
		});
	};

	$scope.getUserPreferences = function() {
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id')
		};
		$http({
			url : 'http://37.139.13.79:9001/preferences',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				} else {
					if (k === "daylimit") {
						$scope.calorypreferences = v;
					}
				}
			});
			$scope.getUserEntries();
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
		});
	};

	init();

	$scope.ResetToDefault = function() {
		$scope.timeFrom = new Date();
		$scope.timeTo = new Date();
		$scope.dateto = new Date();
		$scope.datefrom = new Date();
		$scope.getUserEntries();
	};

	$scope.GetEntriesByTime = function() {
		var hours = $scope.timeFrom.getHours();
		var minutes = $scope.timeFrom.getMinutes();
		var seconds = $scope.timeFrom.getSeconds();
		var monthto = $scope.dateto.getMonth() + 1;
		var monthfrom = $scope.datefrom.getMonth() + 1;
		$scope.headers = [];
		$scope.items = [];
		$scope.dataForPage = [];
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id'),
			'timeFrom' : $scope.timeFrom.getHours() + ":" + $scope.timeFrom.getMinutes() + ":" + $scope.timeFrom.getSeconds(),
			'timeTo' : $scope.timeTo.getHours() + ":" + $scope.timeTo.getMinutes() + ":" + $scope.timeTo.getSeconds(),
			'dateFrom' : $scope.datefrom.getFullYear() + "-" + monthfrom + "-" + $scope.datefrom.getDate(),
			'dateTo' : $scope.dateto.getFullYear() + "-" + monthto + "-" + $scope.dateto.getDate(),
		};
		$http({
			url : 'http://37.139.13.79:9001/getentriebytime',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				}
			});

			if (error === false) {
				$.each(response, function(k, v) {
					if (v !== "OK") {
						$scope.items.push(v);
					}
				});
				var inHeaders = false;
				$.each($scope.items, function(k, v) {

					$.each($scope.headers, function(key, value) {
						if (v["entry_date"] === value) {
							inHeaders = true;
						}
					});
					if (!inHeaders) {
						$scope.headers.push(v["entry_date"]);
					}
					inHeaders = false;

				});
				$.each($scope.headers, function(k, v) {
					var helper = [];
					var calories = 0;
					$.each($scope.items, function(key, value) {
						if (v === value["entry_date"]) {
							calories = calories + value["calories"];
						}
					});
					var caloriesdata = {
						'totalcalories' : calories
					};
					helper.push(caloriesdata);
					$.each($scope.items, function(key, value) {
						if (v === value["entry_date"]) {
							helper.push(value);
						}
					});
					$scope.dataForPage.push(helper);
				});
			}
			console.log("$scope.items.length:" + $scope.items.length);
			if ($scope.items.length > 0) {
				$scope.notime = false;
			} else {
				$scope.notime = true;
			}
			$scope.groupToPages();
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
		});
	};

}).controller('MealControler', function($scope, $http, $cookieStore, $location) {

	$scope.testcalories = 1;

	$scope.loading = false;

	$scope.mytime = new Date();
	$scope.datepicker = new Date();

	$scope.hstep = 1;
	$scope.mstep = 5;

	$scope.ismeridian = true;
	$scope.toggleMode = function() {
		$scope.ismeridian = !$scope.ismeridian;
	};

	$scope.addMeals = function() {
		$scope.loading = true;
		var error = true;
		var data = {
			'userid' : $cookieStore.get('id'),
			'name' : $scope.name,
			'memo' : $scope.memo,
			'calories' : $scope.testcalories,
			'houres' : $scope.mytime.getHours() + ":" + $scope.mytime.getMinutes() + ":" + $scope.mytime.getSeconds(),
			'date' : $scope.datepicker
		};
		$http({
			url : 'http://37.139.13.79:9001/setentrie',
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json'
			},
			data : JSON.stringify(data)
		}).success(function(response) {
			console.log('Res', response);
			$.each(response, function(k, v) {
				if (k === "status") {
					if (v === "OK") {
						error = false;
					}
				}
				if (error === true) {
					if (k === "message") {
						$scope.errormessage = v;
					}
				} else {
					if (k === "daylimit") {
						console.log("God id for user: " + v);
						$scope.preferences = v;
					}
				}
			});
			$scope.loading = false;
			$location.path('/login');
		}).error(function(err) {
			console.log('err', err);
			$scope.errormessage = "Error during server comunication";
			$scope.loading = false;
		});
	};

	var init = function() {
		var id = $cookieStore.get('id');
		if ( typeof id == 'undefined') {
			$location.path('/login');
		}
	};

	$scope.LogOut = function() {
		$cookieStore.remove('id');
		$location.path('/');
	};

	init();
});
