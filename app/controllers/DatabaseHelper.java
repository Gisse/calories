package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.TimeZone;

import javax.sql.DataSource;

import com.fasterxml.jackson.databind.node.ObjectNode;

import play.Logger;
import play.db.*;
import play.libs.Json;

public class DatabaseHelper {

	private DataSource ds;
	private Connection connection;

	private static int CALORY_DEFAULT_LIMIT = 1500;

	public DatabaseHelper() {
		ds = DB.getDataSource();
		TimeZone tz = TimeZone.getTimeZone("UTC");
	}

	public void GetData() {
		connection = DB.getConnection();
		Statement statement = null;
		String sql;
		sql = "SELECT username, userpassword FROM calories.users";

		try {
			statement = connection.createStatement();
			ResultSet rs = statement.executeQuery(sql);
			int i = 1;
			while (rs.next()) {

				Logger.info("Collecting password and username for column number: "
						+ i);
				String username = rs.getString("username");
				Logger.info("Username: " + username);
				String password = rs.getString("userpassword");
				Logger.info("Password: " + password);
				i++;
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ObjectNode registerUser(String username, String password) {
		connection = DB.getConnection();
		Statement statementinsert = null;
		Statement statementselect = null;
		Statement statementcheck = null;
		int userID = 0;
		Logger.info("DatabaseHelper.registerUser");
		ObjectNode result = Json.newObject();
		String sqlinsert, sqlselect, sqlcheck;
		ResultSet rsselect, rscheck;
		sqlcheck = "SELECT userid FROM `calories`.`users` WHERE username = '"
				+ username + "';";
		sqlinsert = "INSERT INTO `calories`.`users` (`username`, `userpassword`) VALUES ('"
				+ username + "', '" + password + "');";
		sqlselect = "SELECT userid FROM `calories`.`users` WHERE username = '"
				+ username + "' AND userpassword = '" + password + "';";

		try {
			statementcheck = connection.createStatement();
			statementinsert = connection.createStatement();
			statementselect = connection.createStatement();
			rscheck = statementcheck.executeQuery(sqlcheck);
			if (rscheck.next()) {
				result.put("status", "ERROR");
				result.put("message",
						"Username already in use, please select another one");
			} else {
				int rsinsert = statementinsert.executeUpdate(sqlinsert);
				rsselect = statementselect.executeQuery(sqlselect);
				while (rsselect.next()) {
					result.put("status", "OK");
					result.put("id", rsselect.getInt("userid"));
					userID = rsselect.getInt("userid");
				}
				insertUserPreferences(userID, CALORY_DEFAULT_LIMIT);
			}
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during registration process");
		}
		return result;
	}

	public ObjectNode getUser(String username, String password) {
		connection = DB.getConnection();
		Statement statement = null;
		ObjectNode result = Json.newObject();
		String sqlselect;
		ResultSet rsselect;
		sqlselect = "SELECT userid FROM `calories`.`users` WHERE username = '"
				+ username + "' AND userpassword = '" + password + "';";

		try {
			statement = connection.createStatement();
			rsselect = statement.executeQuery(sqlselect);

			while (rsselect.next()) {
				result.put("status", "OK");
				result.put("id", rsselect.getInt("userid"));
			}
			if (result.size() == 0) {
				result.put("status", "ERROR");
				result.put("message", "Check login credential");
			}

			rsselect.close();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during login process");
		}
		return result;
	}

	public ObjectNode getUserPreferences(int userid) {
		connection = DB.getConnection();
		Statement statement = null;
		ObjectNode result = Json.newObject();
		String sqlselect;
		ResultSet rsselect;
		sqlselect = "SELECT limit_value FROM `calories`.`daylimit` WHERE userid = '"
				+ userid + "';";

		try {
			statement = connection.createStatement();
			rsselect = statement.executeQuery(sqlselect);
			while (rsselect.next()) {
				result.put("status", "OK");
				result.put("daylimit", rsselect.getInt("limit_value"));
			}
			if (result.size() == 0) {
				result.put("status", "ERROR");
				result.put("message", "Check login credential");
			}
			rsselect.close();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error obtaining user preferences");
		}
		return result;
	}

	public ObjectNode insertUserPreferences(int userid, int daylimit) {
		connection = DB.getConnection();
		Statement statement = null;
		ObjectNode result = Json.newObject();
		String sqlinsert;
		int rsselect;
		sqlinsert = "INSERT INTO `daylimit`(`limit_value`, `userid`) VALUES ("
				+ daylimit + "," + userid + ");";
		try {
			statement = connection.createStatement();
			rsselect = statement.executeUpdate(sqlinsert);
			result = getUserPreferences(userid);
			connection.close();
		} catch (SQLException e) {

			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during preference update process");
		}
		return result;
	}

	public ObjectNode setUserPreferences(int userid, int daylimit) {
		connection = DB.getConnection();
		Statement statement = null;
		ObjectNode result = Json.newObject();
		String sqlupdate;
		int rsselect;
		sqlupdate = "UPDATE  `daylimit` SET  `limit_value` =" + daylimit
				+ " WHERE  `userid` =" + userid + ";";
		try {
			statement = connection.createStatement();
			rsselect = statement.executeUpdate(sqlupdate);
			result = getUserPreferences(userid);
			connection.close();
		} catch (SQLException e) {

			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during preference update process");
		}
		return result;
	}

	public ObjectNode setUserEntry(int userid, String name, String memo,
			int calories, String houres, String date) {
		connection = DB.getConnection();
		ObjectNode result = Json.newObject();
		String insertString;
		Date dbdate = null;
		Timestamp dbdatestamp;
		PreparedStatement insertEntry;

		try {
			dbdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
					Locale.ENGLISH).parse(date);
			dbdatestamp = new Timestamp(dbdate.getTime());

			insertString = "INSERT INTO `entries`(`entry_date`, `entry_time`, `entry_name`, `calories`, `user_id`, `entry_text`) VALUES (?,?,?,?,?,?)";
			connection.setAutoCommit(false);
			insertEntry = connection.prepareStatement(insertString);
			insertEntry.setTimestamp(1, dbdatestamp);
			insertEntry.setString(2, houres);
			insertEntry.setString(3, name);
			insertEntry.setInt(4, calories);
			insertEntry.setInt(5, userid);
			insertEntry.setString(6, memo);
			insertEntry.executeUpdate();
			connection.commit();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during entry input process");
		} catch (ParseException e){
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Unable to parse data for query");
		}

		return result;
	}

	public ObjectNode getUserEntries(int userid) {
		connection = DB.getConnection();
		Statement statement = null;

		ObjectNode result = Json.newObject();
		String sqlselect;
		ResultSet rsselect;
		int i = 0;
		sqlselect = "SELECT * FROM  `entries` WHERE  `user_id` ='" + userid
				+ "' ORDER BY  `entry_date`;";

		try {
			statement = connection.createStatement();
			rsselect = statement.executeQuery(sqlselect);
			result.put("status", "OK");
			while (rsselect.next()) {
				ObjectNode resultlist = Json.newObject();
				resultlist.put("name", rsselect.getString("entry_name"));
				resultlist.put("memo", rsselect.getString("entry_text"));
				resultlist.put("calories", rsselect.getInt("calories"));
				resultlist.put("entry_date",
						new SimpleDateFormat("MMM/dd/yyyy").format(rsselect
								.getTimestamp("entry_date")));
				resultlist.put("entry_time", rsselect.getTime(("entry_time"))
						.toString());
				result.put(i + "", resultlist);
				i++;
			}
			rsselect.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Error during login process");
		}
		Logger.info("DatabaseHelper.getUserEntries result: "
				+ result.toString());
		return result;
	}

	public ObjectNode getListofEntriesByTime(int userid, String timefrom,
			String timeto, String datefrom, String dateto) {
		connection = DB.getConnection();
		Statement statement = null;
		ObjectNode result = Json.newObject();
		String sqlselect;
		ResultSet rsselect;
		int i = 0;
		try {
			sqlselect = "SELECT * FROM  `entries` WHERE  `user_id` = " + userid
					+ " AND  `entry_time` >=  \"" + timefrom
					+ "\" AND  `entry_time` <=  \"" + timeto
					+ "\" AND `entry_date` >=  \"" + datefrom
					+ "\" AND  `entry_date` <=  \"" + dateto
					+ "\" ORDER BY  `entry_date`;";
			statement = connection.createStatement();
			rsselect = statement.executeQuery(sqlselect);
			connection.close();

			result.put("status", "OK");
			while (rsselect.next()) {
				ObjectNode resultlist = Json.newObject();
				resultlist.put("name", rsselect.getString("entry_name"));
				resultlist.put("memo", rsselect.getString("entry_text"));
				resultlist.put("calories", rsselect.getInt("calories"));
				resultlist.put("entry_date",
						new SimpleDateFormat("MMM/dd/yyyy").format(rsselect
								.getTimestamp("entry_date")));
				resultlist.put("entry_time", rsselect.getTime(("entry_time"))
						.toString());
				result.put(i + "", resultlist);
				i++;
			}
			rsselect.close();
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
			result.put("status", "ERROR");
			result.put("message", "Unable to get entries");
		}
		Logger.info("DatabaseHelper.getListofEntriesByTime result: "
				+ result.toString());
		return result;
	}

	public String getDateString(Timestamp stamp) {
		String datestring = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(stamp);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		datestring = "" + month + "/" + day + "/" + year;
		return datestring;
	}

	public String getTimeString(Time stamp) {
		String datestring = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(stamp);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		int second = cal.get(Calendar.SECOND);
		datestring = "" + hour + ":" + minute + ":" + second;
		return datestring;
	}

}
