package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.*;
import play.libs.Json;
import play.mvc.*;
import views.html.*;

public class Application extends Controller {

	public static Result index() {
		return ok(index.render("Your new application is ready."));
	}

	public static Result database() {
		DatabaseHelper data = new DatabaseHelper();
		data.GetData();
		return ok(index.render("Your new application is ready."));
	}
	
	public static Result checkPreFlight() {
	    response().setHeader("Access-Control-Allow-Origin", "*");       // Need to add the correct domain in here!!
	    response().setHeader("Access-Control-Allow-Methods", "POST");   // Only allow POST
	    response().setHeader("Access-Control-Max-Age", "300");          // Cache response for 5 minutes
	    response().setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");         // Ensure this header is also allowed!  
	    return ok();
	}

	@BodyParser.Of(BodyParser.Json.class)
	public static Result registerUser() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		String username = json.findPath("username").textValue();
		String password = json.findPath("password").textValue();
		if (username == null) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [username]");
			return badRequest(result);
		} else if (password == null) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [password]");
			return badRequest(result);
		} else {
			result = data.registerUser(username, password);
			return ok(result);
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result loginUser() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		String username = json.findPath("username").textValue();
		String password = json.findPath("password").textValue();
		if (username == null) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [username]");
			return badRequest(result);
		} else if (password == null) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [password]");
			return badRequest(result);
		} else {
			result = data.getUser(username, password);
			return ok(result);
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result getUserPreferences() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		int userid = json.findPath("userid").intValue();
		if (userid == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [userid]");
			return badRequest(result);
		} else {
			result = data.getUserPreferences(userid);
			return ok(result);
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result setUserPreferences() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		int userid = json.findPath("userid").intValue();
		int daylimit = Integer.parseInt(json.findPath("daylimit").textValue());
		Logger.info("Application.setUserPreferences userid: "+userid+" daylimit:"+daylimit );
		if (userid == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [userid]");
			return badRequest(result);
		} else if (daylimit == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [daylimit]");
			return badRequest(result);
		} else {
			result = data.setUserPreferences(userid, daylimit);
			return ok(result);
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result getListofEntries() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		int userid = json.findPath("userid").intValue();
		if (userid == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [userid]");
			return badRequest(result);
		} else {
			result = data.getUserEntries(userid);
			return ok(result);
		}
	}
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result getListofEntriesByTime() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		int userid = json.findPath("userid").intValue();
		String timefrom = json.findPath("timeFrom").asText();
		String timeto = json.findPath("timeTo").asText();
		String datefrom = json.findPath("dateFrom").asText();
		String dateto = json.findPath("dateTo").asText();
		if (userid == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [userid]");
			return badRequest(result);
		} else {
			result = data.getListofEntriesByTime(userid, timefrom, timeto, datefrom, dateto);
			return ok(result);
		}
	}
	
	
	@BodyParser.Of(BodyParser.Json.class)
	public static Result saveEntrie() {
		response().setHeader("Access-Control-Allow-Origin", "*");
		DatabaseHelper data = new DatabaseHelper();
		ObjectNode result = Json.newObject();
		JsonNode json = request().body().asJson();
		int userid = json.findPath("userid").intValue();
		String name = json.findPath("name").asText();
		String memo = json.findPath("memo").asText();
		int calories = Integer.parseInt(json.findPath("calories").asText());
		String houres = json.findPath("houres").asText();
		String date = json.findPath("date").asText();
		Logger.info("Application.saveEntrie userid: "+userid+" name:"+name+" memo:"+memo+" calories:"+calories+" houres:"+houres+" date:"+date );
		if (userid == 0) {
			result.put("status", "ERROR");
			result.put("message", "Missing parameter [userid]");
			return badRequest(result);
		}else {
			result = data.setUserEntry(userid, name, memo, calories, houres, date);
			return ok(result);
		}
	}
}
