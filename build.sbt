name := "calmeter"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)     

play.Project.playJavaSettings

libraryDependencies ++= Seq(
  "mysql" % "mysql-connector-java" % "5.1.30"
)